tree(empty).

tree(node(_, Left, Right)):-
    tree(Left),
    tree(Right).

member_tree(Target, node(Target, _, _)). % target found

member_tree(Target, node(NotTarget, Left, _)):-
    NotTarget \= Target,
    member_tree(Target, Left). % search in left branch

member_tree(Target, node(NotTarget, _, Right)):-
    NotTarget \= Target,
    member_tree(Target, Right). % search in right branch

%==========================================

preorder(empty, []).

preorder(node(Root, Left, Right), [Root | Rest]):-
    preorder(Left, LeftList),
    preorder(Right, RightList),
    append(LeftList, RightList, Rest).
%% %====================================

postorder(empty, []).

postorder(node(Root, Left, Right), List):-
    postorder(Left, LeftList),
    postorder(Right, RightList),
    append(LeftList, RightList, LeftRightList),
    append(LeftRightList, [Root], List).

%====================================
compare(node(Atom, _, _), Atom). % check this node's value

not_empty(node(Atom, _, _)):-
    Atom \= empty.

roots([],[]). %% stop condition

roots([TreeHead | TreeRest], [RootHead | RootRest]):-
    %% TreeHead must not be an empty tree
    not_empty(TreeHead),

    %% Compare its root with RootHead
    compare(TreeHead, RootHead),

    %% Compare the other tree
    roots(TreeRest, RootRest).

%% else TreeHead is an empty tree, omit it.
roots([_ | TreeRest], List):-
    roots(TreeRest, List).

%% sample tree:
%% node(root1, node(left_node, empty, node(left_right, empty, empty)), node(right_node, node(right_left, empty, empty), node(right_right, empty, empty)))

%% 3 normal tree:
%% ?- roots([node(root1, node(left_node, empty, node(left_right, empty, empty)), node(right_node, node(right_left, empty, empty), node(right_right, empty, empty))), node(root2, empty, empty), node(root3, node(left3, empty, empty), node(right3, empty, empty))], What).

%% normal tree, empty tree, normal tree:
%% ?- roots([node(root1, node(left_node, empty, node(left_right, empty, empty)), node(right_node, node(right_left, empty, empty), node(right_right, empty, empty))), node(empty, empty, empty), node(root3, node(left3, empty, empty), node(right3, empty, empty))], What).
