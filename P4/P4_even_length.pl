%% L even length.

%% allow only these states:
state(z0).
state(z1).
%% state(z2_end).

%% allow only these characters:
alphabet(a).
alphabet(b).

%% allow only these transitions:
delta_f(z0, a, z1).
delta_f(z0, b, z1).
delta_f(z1, a, z0).
delta_f(z1, b, z0).
%% delta_f(z1, a, z2_end).
%% delta_f(z1, b, z2_end).

%% only these states can morph into another state
start(z0).

% only ends with this state
end(z0).

%% relations:
%% generated words set (sigma*): each character in this word must exist in the base alphabet set
sigma_star([]).
sigma_star([Atom|Rest]):-
    alphabet(Atom),
    sigma_star(Rest).

%% transitional relations:
delta_set(Z_current, [], Z_current). %% stop condition, reached the end of the list
delta_set(Z_current, [Atom|Rest], Z_last):-
    %% start(Z_current), %% z must be eligible to start a transition
    delta_f(Z_current, Atom, Z_temp), %% check this transition with z and atom
    delta_set(Z_temp, Rest, Z_last). %% check other atoms in list
    

l_of_n(List):-
    sigma_star(List), %% firstly, all atoms must be valid
    start(Z_start),
    delta_set(Z_start, List, Z_last),
    state(Z_last),
    end(Z_last).


%% %% validate the word (list)
%% sigma_star([]).
%% sigma_star([Atom|Rest]):-
%%     alphabet(Atom),
%%     sigma_star(Rest).

%% delta_s(Z_current, [], Z_current). %% the end of the word (list)
%% delta_s(Z_current, [Atom|Rest], Z_last):-
%%     start(Z_current),
%%     delta_f(Z_current, Atom, Z_temp),
%%     delta_s(Z_temp, Rest, Z_last).

%% language(List):-
%%     sigma_star(List),
%%     delta_s(z0, List, Z_last), %% z0 must be the fisrt state
%%     end(Z_last).