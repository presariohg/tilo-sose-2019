is_male(grandpa).
is_male(dad).
is_male(uncle).
is_male(son).

is_female(mom).
is_female(aunt).
is_female(daughter).
is_female(cousin).

is_parent(dad, son).            % dad is son's parent
is_parent(dad, daughter).       % dad is daughter's parent
is_parent(mom, son).            % mom is son's parent
is_parent(mom, daughter).       % mom is daughter's parent
is_parent(uncle, cousin).       % uncle is cousin's parent
is_parent(grandpa, dad).        % grandpa is dad's parent
is_parent(grandpa, uncle).      % grandpa is uncle's parent
is_parent(grandpa, aunt).       % grandpa is aunt's parent


%% Spouses have mutual child
is_spouse(Bride, Groom):-
    is_parent(Bride, Child),
    is_parent(Groom, Child),
    Bride \= Groom.


%% Parent in law is spouse's parent
is_parent_in_law(ParentInLaw, Person):-
    is_parent(ParentInLaw, Spouse),
    is_spouse(Person, Spouse).


%% Father in law is spouse's father
is_father_in_law(Person, FatherInLaw):-
    is_parent_in_law(FatherInLaw, Person),
    is_male(FatherInLaw).


%% 2 people are siblings if they have the same parent
is_siblings(Person1, Person2):-
    is_parent(Parent, Person2),
    is_parent(Parent, Person1),
    Person1 \= Person2.


%% Sister is one's female siblings
is_sister(Person1, Person2):-
    is_siblings(Person1, Person2),
    is_female(Person1).


%% Brother is one's male siblings
is_brother(Brother, Person):-
    is_siblings(Brother, Person),
    is_male(Brother).


%% Sister in law is siblings's wife
is_sister_in_law(SisterInLaw, Person):-
    is_siblings(Person, Sibling),
    is_spouse(SisterInLaw, Sibling).


%% Mother is one's female parent 
is_mother(Mother, Child):-
    is_parent(Mother, Child),
    is_female(Mother).


%% Father is one's male parent 
is_father(Father, Child):-
    is_parent(Father, Child),
    is_male(Father).


%% Aunt is parent's sister
is_aunt(Aunt, Child):-
    is_sister(Aunt, Parent),
    is_parent(Parent, Child).


% Uncle is parent's brother
is_uncle(Uncle, Child):-
    is_brother(Uncle, Parent),
    is_parent(Parent, Child).


%% Child is Parent's offspring if Parent is Child's parent
is_offspring(Child, Parent):-
    is_parent(Parent, Child).


%% Daughter is Parent's female offspring
is_daughter(Daughter, Parent):-
    is_offspring(Daughter, Parent),
    is_female(Daughter).


%% Son is parent's male offspring
is_son(Son, Parent):-
    is_offspring(Son, Parent),
    is_male(Son).


%% Daughter in law is offspring's wife
is_daughter_in_law(DaughterInLaw, Person):-
    is_spouse(DaughterInLaw, Offspring),
    is_parent(Person, Offspring).


%% Nephew is sibling's son
is_nephew(Nephew, Person):-
    is_siblings(NephewsParent, Person),
    is_son(Nephew, NephewsParent).


%% Niece is sibling's daughter
is_niece(Niece, Person):-
    is_siblings(NiecesParent, Person),
    is_daughter(Niece, NiecesParent).


%% Cousin is offspring of a parent's sibling
is_cousin(Person, Cousin):-
    is_parent(ParentsSibling, Cousin),
    is_parent(Parent, Person),
    is_siblings(Parent, ParentsSibling).


%% Grandparent is parent's parent
is_grandparent(Grandparent, Child):-
    is_parent(Grandparent, Parent),
    is_parent(Parent, Child).


%% Grandfather is a male grandparent
is_grandfather(Grandfather, Child):-
    is_grandparent(Grandfather, Child),
    is_male(Grandfather).
