%% :- style_check(-singleton).

% kinda like a linked list, each list contain a data and a link to the next list. 
% the last list points to nil, which is equivalent to null
list(nil).
list(_, nil).
list(_, list(_, Next)):-
    list(Next).

member(X, X).

member(Keyword, list(Data, _)):-
    member(Keyword, Data).

member(Keyword, list(_, Next)):-
    member(Keyword, Next).


% match last atom in sublist
contain(list(Data, _), list(Data, nil)).
contain(list(_, _), nil).

% atoms dont match, move to main list's next list.
contain(list(Data, Next), list(SubData, SubNext)):-
    Data \= SubData,
    contain(Next, SubNext).

% atoms match, move to next list on both list.
contain(list(Data, Next), list(Data, SubNext)):-
    contain(Next, SubNext).

%% ?- contain(list(1, list(2, nil)), list(3, nil)). (False)




% stop when reaches the end of the list, return a new list with new atom attached at the end.
attach(list(Data, nil), NewAtom, list(Data, list(NewAtom, nil))).

attach(list(Data, Next), NewAtom, list(Data, NewNext)):-
    attach(Next, NewAtom, NewNext).

%% ?- attach(list(0, list(1, nil)), 2, What). 


reverse(list(X, nil), list(X, nil)). % reverse of one single atom is itself


reverse(list(Data, Next), list(ReversedData, ReversedNext)):-

    % cut this atom out, find the reverse of the rest recursively
    reverse(Next, list(RestDataReversed, RestNext)), 

    % attach the reversed rest found above with current atom to continue the reversed list
    attach(list(RestDataReversed, RestNext), Data, list(ReversedData, ReversedNext)). 

%% ?- reverse(list(3, list(2, list(1, nil))), What).
