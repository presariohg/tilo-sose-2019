:- style_check(-singleton).

is_tree(nil).

% every node is a mini tree
is_tree(node(_, Left, Right)):- 
    is_tree(Left),
    is_tree(Right).
    
%integer(root)?

nat_symb(o).

nat_symb(s(X)):- 
    nat_symb(X).

add(o, X, X). %X + o = X

add(s(X), Y, s(Z)):-
    add(X, Y, Z), 
    nat_symb(X), 
    nat_symb(Y), 
    nat_symb(Z).

% empty node does not count
node_count(nil, o).

% total node is the sum of all branches's node. leaf (nodes with no children) count as 1 node.
node_count(node(_, Left, Right), s(N)):-
    node_count(Left, LeftCount),
    node_count(Right, RightCount),
    add(LeftCount, RightCount, N).
    %% Counter is LeftCount + RightCount + 1.

%% is_tree(Root, Left, Right).

construct(Root, Left, Right, Tree):-
    Tree = node(Root, Left, Right),
    is_tree(Left),
    is_tree(Right).

%% ?- node_count(node(whatev, node(left_node, nil, node(left_right, nil, nil)), nil), HowMany).
%% HowMany = 3.

%% ?- construct(0, Left, node(r, nil, nil), node(0, node(l, node(ll, nil, nil), nil), node(r, nil, nil))).
%% Left = node(l, node(ll, nil, nil), nil).