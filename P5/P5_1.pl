%% L = 0^n1^n (empty, 01, 0011, 000111,...)
%% S -> epsilon, S -> 0S1

%% Big Z
state(z0). 
state(z1).
state(z2_end). % PDAs don't have end states?

%% Big Sigma
alphabet(0).
alphabet(1).

%% Big Gamma
alphabet_stack(0).
alphabet_stack(1).
alphabet_stack(#).

%% Transition relations, small deltas
%% delta(current_state, input, stack.pop(), stack.push(), next_state())
delta(z0, 0, #, [0, #], z0). %% meet 0, have none => push 0 into stack, next = z0
delta(z0, 0, 0, [0, 0], z0).    %% meet 0, have 0 => push one more 0 into stack, next = z0 (a 0 has already been poped out, so we need to push 2 0s here)
delta(z0, 1, 0, [], z1).        %% meet 1, have 0 => push nothing into stack, next = z1
                                %% whatever => false (z0 never accepts single '1's)
delta(z1, 1, 0, [], z1).      %% meet 1, have 0 => push nothing into stack, next = z1
delta(z1, [], #, [], z2_end). %% meet the end of the word & have nothing left => next = z2, end, true.
                              %% meet whatever else => false.  
%% delta()

start(z0).
%% end(z2_end). PDAs don't have end states?

%% Sigma star, check if this word contains invalid characters
sigma_s([]).
sigma_s([Current_Char | Rest]):-
    alphabet(Current_Char),
    sigma_s(Rest).

%% Transition relations, step by step through each char in the word

% es(current z, current word, word after removing first char, current stack, stack after transitioning, next z).


% reached the end of the word, current char = current word = rest word = []
es(Z_current, [], [], [Stack_pop | Stack_rest], Stack, Z_last):-
    delta(Z_current, [], Stack_pop, Stack_push, Z_last),
    append(Stack_push, Stack_rest, Stack).

es(Z_current, [Char_current | Word_rest], Word_rest, [Stack_pop | Stack_rest], Stack, Z_next):-
    delta(Z_current, Char_current, Stack_pop, Stack_push, Z_next),
    append(Stack_push, Stack_rest, Stack).

% es_p(current z, current word, word after removing first char, current stack, stack after transitioning, next z).

es_p(Z_current, Word, Word_rest, Stack, Stack_rest, Z_next):-
    es(Z_current, Word, Word_rest, Stack, Stack_rest, Z_next).

es_p(Z_current, Word, Word_rest, Stack, Stack_rest, Z_last):-
    es(Z_current, Word, Word_next, Stack, Stack_next, Z_next),
    es_p(Z_next, Word_next, Word_rest, Stack_next, Stack_rest, Z_last).

l_of_m(Word):-
    sigma_s(Word),
    start(Z_start),
    
    % find a start state, start with Word, end with empty word, start with # in stack, end with nothing in stack, and find an end state
    es_p(Z_start, Word, [], [#], [], Z_last),

    state(Z_last).