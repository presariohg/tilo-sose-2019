%% L = palindrom with separator (x, 0x0, 1x1, 01x10,...), choose 'x' as separator because x is vertically symmetric and short
%% S -> epsilon, S -> x, S -> 0S0, S -> 1S1

%% Big Z
state(z0). 
state(z1).
state(z2).
state(z3_end).

%% Big Sigma
alphabet(0).
alphabet(1).
alphabet(x).

%% Big Gamma
alphabet_stack(0).
alphabet_stack(1).
alphabet_stack(#).

%% Transition relations, small deltas
%% delta(current_state, input, stack.pop(), stack.push(), next_state())
delta(z0, 0, #, [0, #], z1). 
delta(z0, 1, #, [1, #], z1). 
delta(z0, x, #, [#], z2). 

%% delta(z1, 0, X, [0, X], z1). X belongs to sigma?
delta(z1, 0, 0, [0, 0], z1).
delta(z1, 0, 1, [0, 1], z1).
delta(z1, 1, 0, [1, 0], z1).
delta(z1, 1, 1, [1, 1], z1).
delta(z1, x, 0, [0], z2).
delta(z1, x, 1, [1], z2).

%% delta(z2, 0, X, [0, X], z2).
delta(z2, 0, 0, [], z2).
delta(z2, 1, 1, [], z2).
delta(z2, [], #, [], z3_end).

start(z0).

%% Sigma star, check if this word contains invalid characters
sigma_s([]).
sigma_s([Current_Char | Rest]):-
    alphabet(Current_Char),
    sigma_s(Rest).

%% Transition relations, step by step through each char in the word
es(Z_current, Word, Word, [Stack_Pop | Stack_Rest], Stack, Z_last):-
    delta(Z_current, [], Stack_Pop, Stack_Push, Z_last),
    append(Stack_Push, Stack_Rest, Stack).

es(Z_current, [Char_current | Word_Rest], Word_Rest, [Stack_Pop | Stack_Rest], Stack, Z_next):-
    delta(Z_current, Char_current, Stack_Pop, Stack_Push, Z_next),
    append(Stack_Push, Stack_Rest, Stack).

es_p(Z_current, Word, Word_Rest, Stack, Stack_Rest, Z_next):-
    es(Z_current, Word, Word_Rest, Stack, Stack_Rest, Z_next).

es_p(Z_current, Word, Word_Rest, Stack, Stack_Rest, Z_last):-
    es(Z_current, Word, Word_next, Stack, Stack_next, Z_next),
    es_p(Z_next, Word_next, Word_Rest, Stack_next, Stack_Rest, Z_last).

l_of_m(Word):-
    sigma_s(Word),
    start(Z_start),
    es_p(Z_start, Word, [], [#], [], Z_last),
    state(Z_last).